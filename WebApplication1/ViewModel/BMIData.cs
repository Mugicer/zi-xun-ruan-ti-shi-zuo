﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModel
{
    public class BMIData
    {
        [Display(Name ="體重")]
        [Required(ErrorMessage = "Can't be null!!!")]
        [Range(30,50,ErrorMessage ="30-50")]
        public float Weight { get; set; }
        [Display(Name = "身高")]
        [Required(ErrorMessage = "Can't be null!!!")]
        [Range(50, 250, ErrorMessage = "50-250")]
        public float Height { get; set; } 
        public float? BMI { get; set; }
        public string Level { get; set; }
    }
}