﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class ArrayController : Controller
    {
        string[] str = {"00", "01", "02", "03" };
        public ActionResult DoIt()
        {
            TempData["msg"] = "hello";
            return RedirectToAction("Index");
        }
        public ActionResult Index()
        {
            ViewBag.nonono = str;
            ViewData["nonono"] = str;
            return View();
        }
        
    }
}