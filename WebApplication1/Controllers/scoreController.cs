﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class scoreController : Controller
    {
        // GET: score
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(float score) {
            string ans="";
            if (80 <= score && score <= 100)
            {
                ans = "A";
            }
            else if (60 <= score && score < 80)
            {
                ans = "B";
            }
            else if (40 <= score && score < 60)
            {
                ans = "C";
            }
            else if (20 <= score && score < 40)
            {
                ans = "D";
            }
            else if (0 <= score && score < 20)
            {
                ans = "E";
            }

            @ViewBag.ans = ans;
            @ViewBag.sc = score;
            return View();
        }
    }
}